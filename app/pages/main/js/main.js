var categoriesComponent =
  require('../../../common/components/categories/js/categories.js');
var productsComponent =
  require('../../../common/components/products/js/main.js');
var productCount = require('../../../common/components/counter/js/counter.js');
var filters = require('../../../common/components/filter/js/filter.js');
var mediator = require('../../../common/mediator/js/mediator.js');
var sortComponent = require('../../../common/components/sort/js/sort.js');
require('../../../common/sass/style.scss');


// main page logic
// initialize the sort component
sortComponent.init();

// initialize the categories list
categoriesComponent.init();

// initialize the product list
productsComponent.init();

// count the numbers of products in the basket
// when the page loads
productCount.init();

// populate categories list
mediator.getCategories();

// populate product list
mediator.getProducts();

// load the cart (to get the product number)
mediator.loadCart();

// bind filter actions
filters.attachFilterEvents();
