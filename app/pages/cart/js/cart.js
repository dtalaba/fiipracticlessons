//cart page requirements --
var events = require('../../../common/events/js/events.js');
var productsCartComponent =
  require('../../../common/components/products/js/cart.js');
var order = require('../../../common/components/order/js/order.js');
var mediator = require('../../../common/mediator/js/mediator.js');

order.init(document.getElementById('cartActions'));

events.subscribe(events.types.cart.load, function onCartLoad() {
  var ids = [];
  mediator.cart.forEach(function getProductInfo(product) {
    ids.push(product.id);
  });
  mediator.getProducts({'id': ids});
});


productsCartComponent.initCart();
mediator.loadCart();
