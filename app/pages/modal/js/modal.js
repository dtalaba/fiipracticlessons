var dom = require('../../../common/utils/dom/js/dom.js');
var mediator = require('../../../common/mediator/js/mediator.js');
var notification =
  require('../../../common/components/notification/js/notification.js');


module.exports = (function modalFutureModule() {

  // Create global element references
  var modal = null,
    overlay = null,
    closeBtn = null,
    modalParent = document.body,

    //options
    animationClass = 'fadeInDown',
    overlayOption = true,
    product = null;

  // Public Methods
  function open(prod) {
    var addToCart = null;

    product = prod;
    buildModal(product);

    modal.classList.add(animationClass, 'modal-open');
    closeBtn.addEventListener('click', closeModal);
    addToCart = document.getElementById('add_to_basket');

    if (overlayOption) {
      overlay.classList.add('modal-open', animationClass);
      overlay.addEventListener('click', closeModal);
    }

    if(addToCart) {
      addToCart.addEventListener('click', function notify() {
        closeModal();
        notification.show('Product added to cart', modalParent);
      });
    }
  }

  function closeModal() {
    modal.classList.remove('modal-open');
    overlay.classList.remove('modal-open');
    document.body.removeChild(modal);
    document.body.removeChild(overlay);
  }

  function buildModal() {
    //generate specs for this product
    var prodSpecs = product.specs,
      singleSpec,
      specsContent = [];

    Object.keys(prodSpecs).forEach(function spec(key) {
      singleSpec = dom({
        'type': 'li',
        'content': [
          dom({
            'type': 'span',
            'content': key + ':'
          }),
          dom({
            'type': 'p',
            'content': prodSpecs[key]
          }),
        ]
      });
      specsContent.push(singleSpec);
    });

    // Add a close button
    closeBtn = dom({
      'type': 'button',
      'attributes': {
        'id': 'closeModal'
      },
      'content': [
        dom({
          'type': 'i',
          'attributes': {
            'class': 'fa fa-times-circle-o'
          }
        })
      ]
    });

    // If overlayOption is true, add overlay behind the modal
    if (overlayOption) {
      overlay = dom({
        'type': 'div',
        'attributes': {
          'class': ['modal-overlay ', animationClass].join('')
        }
      }, modalParent);
    }

    //Build main modal
    modal = dom({
      'type': 'div',
      'attributes': {
        'class': ['modal ', animationClass].join('')
      },
      'content': [
        dom({
          'type': 'div',
          'attributes': {
            'class': 'modal__container',
          },
          'content': [
            dom({
              'type': 'div',
              'attributes': {
                'class': 'modal__header',
              },
              'content': [
                closeBtn,
                dom({
                  'type': 'h1',
                  'content': product.name
                })
              ]
            }),
            dom({
              'type': 'div',
              'attributes': {
                'class': 'modal__body'
              },
              'content': [
                dom({
                  'type': 'div',
                  'attributes': {
                    'class': 'gallery'
                  },
                  'content': [
                    dom({
                      'type': 'img',
                      'attributes': {
                        'src': product.getThumb()
                      }
                    })
                  ]
                }),
                dom({
                  'type': 'div',
                  'attributes': {
                    'class': 'product_info'
                  },
                  'content': [
                    dom({
                      'type': 'div',
                      'attributes': {
                        'class': 'price'
                      },
                      'content': [
                        dom({
                          'type': 'span',
                          'content': [
                            'Price: ',
                            dom({
                              'type': 'b',
                              'content': ['$', product.getPrice()].join('')
                            })
                          ]
                        })
                      ]
                    }),
                    dom({
                      'type': 'ul',
                      'attributes': {
                        'class': 'specs'
                      },
                      'content': specsContent
                    }),
                    dom({
                      'type': 'button',
                      'attributes': {
                        'id': 'add_to_basket'
                      },
                      'content': 'Add to cart',
                      'events': {
                        'click': function addToCart() {
                          mediator.addToCart(product);
                        }
                      }
                    })
                  ]
                })
              ]
            })
          ]
        }),
      ]
    });


    modalParent.appendChild(modal);
  }


  return {
    'open': open
  };

}());
