var dom = require('../../../utils/dom/js/dom.js');
var events = require('../../../events/js/events.js');
var notification =
  require('../../../components/notification/js/notification.js');
var productCartComponent = require('../../product/js/cart.js');
var strings = require('../../../utils/config/js/strings.js');
var mediator = require('../../../mediator/js/mediator.js');

module.exports = (function productsCartComponentModule() {

  // the DOM element in which the products' DOM will reside
  var productsContainer = null;

  // this is a private function which we won't expose to the rest of the
  // application
  var insertCartProducts = function insertCartProducts(e) {
    productsContainer.innerHTML = '';

    if (!mediator.getDistinctCartProducts()) {
      productsContainer.appendChild(dom({
        'type': 'p',
        'content': strings.noProdsInCart
      }));
      return;
    }

    e.detail.products.forEach(function insertProduct(product) {
      product.quantity = mediator.getProductFromCart(product.id).quantity;
      productsContainer.appendChild(
        productCartComponent.createCartDOM(product)
      );
    });
  };

  var quantityCartProducts = function quantityCartProducts(e) {
    var input = document.querySelector('[data-id=' +
        e.detail.product.id + '] .quantity');
    input.value = mediator.getProductFromCart(e.detail.product.id).quantity;
  };

  // this function initialize products from cart
  var initCart = function initCartList() {
    productsContainer = dom({
      'type': 'div',
      'attributes': {
        'id': 'productList',
        'class': 'product product--list'
      }
    });

    dom({
      'type': 'section',
      'attributes': {
        'class': 'row products__wrapper'
      },
      'content': [
        productsContainer
      ]
    }, document.getElementById('productsSection'));

    events.subscribe(events.types.products.retrieved, insertCartProducts);
    events.subscribe(events.types.cart.quantity, quantityCartProducts);
    events.subscribe(events.types.cart.ordered, function onOrder(e) {
      notification.show(strings.ordered, document.body);
      insertCartProducts(e);
    });
  };

  return {
    'initCart': initCart
  };

}());
