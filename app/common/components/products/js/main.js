var dom = require('../../../utils/dom/js/dom.js');
var events = require('../../../events/js/events.js');
var productComponent = require('../../product/js/main.js');
var strings = require('../../../utils/config/js/config.js');

module.exports = (function productsComponentModule() {

  // the DOM element in which the products' DOM will reside
  var productsContainer = null;

  // this is a private function which we won't expose to the rest of the
  // application
  var insertProducts = function insertProducts(e) {
    productsContainer.innerHTML = '';

    if (!e.detail.products) {
      productsContainer.appendChild(dom({
        'type': 'p',
        'contents': strings.noProds
      }));
      return;
    }

    e.detail.products.forEach(function insertProduct(product) {
      productsContainer.appendChild(productComponent.createDOM(product));
    });
  };

  var init = function initProductList() {
    productsContainer = dom({
      'type': 'section',
      'attributes': {
        'class': 'row products__wrapper'
      }
    }, document.getElementById('productsSection'));

    events.subscribe(events.types.products.retrieved, insertProducts);
  };

  return {
    'init': init
  };

}());
