/* eslint-disable func-names */

var cartProducts = require('../js/cart.js'),
  dom = require('../../../utils/dom/js/dom.js'),
  events = require('../../../events/js/events.js'),
  mediator = require('../../../mediator/js/mediator.js'),
  Product = require('../../../model/js/product.js'),
  strings = require('../../../utils/config/js/strings.js');

describe('Cart listing tests', function() {

  var products = [];

  beforeAll(function() {
    products = [
      new Product({
        'category': 'phones',
        'id': 'iphone6',
        'name': 'iPhone 6s',
        'specs': {
          'price': 649,
          'memory': 16,
          'color': 'Silver'
        },
        'addons': ['mem64', 'mem128', 'colorGold', 'colorGray', 'colorRose']
      }),
      new Product ({
        'category': 'phones',
        'id': 'nexus6p',
        'name': 'Nexus 6P',
        'specs': {
          'price': 0,
          'memory': 32,
          'color': 'Gray'
        },
        'addons': ['mem64', 'mem128', 'colorBlack', 'colorGold', 'colorGray']
      })
    ];

    dom({
      'type': 'section',
      'attributes': {
        'id': 'productsSection'
      }
    }, document.body);
  });

  it('should load cart correctly', function(done) {
    mediator.addToCart(products[0]);
    mediator.addToCart(products[1]);
    done();
  });

  it('should init listing cart page', function(done) {
    var listener = function() {
      events.unsubscribe(events.types.products.retrieved, listener);
      expect(document.getElementById('productList')).not.toBeNull();
      expect(document.getElementById('productList').children.length).toBe(2);
      done();
    };
    cartProducts.initCart();
    mediator.getProducts({'id': products.map(function(product) {
      return product.id;
    })});
    events.subscribe(events.types.products.retrieved, listener);
  });

  it('should change quantity for products', function(done) {
    events.subscribe(events.types.cart.quantity, function listener() {
      var input = document.querySelector('[data-id=' +
          products[0].id + '] .quantity');
      events.unsubscribe(events.types.cart.quantity, listener);
      expect(input.value).toBe('2');
      done();
    });

    mediator.changeCartQuantity(products[0], +1);
  });

  it('should remove cart products on fire order event', function(done) {
    events.subscribe(events.types.cart.ordered, function listener() {
      events.unsubscribe(events.types.cart.ordered, listener);
      expect(document.getElementById('productList').children[0].innerHTML)
        .toBe(strings.noProdsInCart);
      done();
    });
    events.publish(events.types.cart.order);
  });

  afterAll(function() {
    document.body.innerHTML = '';
  });

});
