var dom = require('../../../utils/dom/js/dom.js');
var events = require('../../../events/js/events.js');

module.exports = (function categoriesComponent() {

  var buildCategoryDOM = function buildCategoryDOM(name) {
    dom({
      'type': 'li',
      'attributes': {
        'class': 'categories__element',
        'data-id': name
      },
      'content': name.toUpperCase(),
      'events': {
        'click': function categoryClick() {
          events.publish(events.types.filter.category, {
            'value': name
          });
        }
      }
    }, document.getElementById('categories'));
  };

  var init = function init() {
    events.subscribe(events.types.categories.retrieved, function build(e) {
      e.detail.categories.forEach(buildCategoryDOM);
    });

    events.subscribe(events.types.products.filtered, function handle(e){
      var selector = null;
      var currentActive =
        document.getElementsByClassName('categories__element active');

      if (e.detail.category) {
        selector =
          document.querySelector('[data-id=' + e.detail.category + ']');
        if (selector) {
          Array.prototype.slice.call(currentActive).forEach(
            function rmv(element) {
              element.classList.remove('active');
            }
          );
          selector.classList.add('active');
        }
      }
    });
  };

  return {
    'init': init
  };

}());
