/* eslint-disable func-names */

var categoryComponent = require('../js/categories.js');
var dom = require('../../../utils/dom/js/dom.js');
var events = require('../../../events/js/events.js');

describe('Categories component tests', function() {

  beforeAll(function() {
    // Required DOM mock creation
    dom({
      'type': 'ul',
      'attributes': {
        'id': 'categories'
      }
    }, document.body);
  });

  // Actual tests
  it('should initialize correctly', function() {
    categoryComponent.init();
  });

  it('should react to the categories retrieved event', function() {
    var fooCategory = null;
    events.publish(events.types.categories.retrieved, {
      'categories': ['foo', 'bar']
    });
    fooCategory = document.querySelector('[data-id=foo]');
    expect(document.getElementById('categories').children.length).toBe(2);
    expect(fooCategory.innerHTML).toBe('FOO');
  });

  it('should allow clicking on categories', function() {
    var elem = document.querySelector('[data-id=foo]');
    var classes = [];
    elem.click();
    classes = Array.prototype.slice.call(elem.classList);
    expect(classes.indexOf('active')).toBe(1);
  });

  it('should not fail when sending an invalid category', function() {
    var elem = document.querySelector('[data-id=foo]');
    var classes = [];
    events.publish(events.types.products.filtered, {});
    events.publish(events.types.products.filtered, {
      'category': 'nothing'
    });
    classes = Array.prototype.slice.call(elem.classList);
    expect(classes.indexOf('active')).toBe(1);
    elem = document.querySelector('[data-id=bar]');
    classes = Array.prototype.slice.call(elem.classList);
    expect(classes.indexOf('active')).toBe(-1);
  });

  afterAll(function() {
    document.body.innerHTML = '';
  });

});
