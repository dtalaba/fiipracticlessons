/* eslint-disable func-names */

var sort = require('../js/sort.js');
var events = require('../../../events/js/events.js');
var dom = require('../../../utils/dom/js/dom.js');

describe('Sort component tests', function() {

  beforeAll(function() {
    // Required DOM mock creation
    dom({
      'type': 'section',
      'attributes': {
        'id': 'sortSection',
        'class': 'row sort'
      }
    }, document.body);

    sort.init();
  });

  it('should create the DOM correctly', function(done) {
    var inputGroup = document.querySelector('.input__group'),
      select = document.getElementById('sort');

    expect(inputGroup.firstChild.nodeName).toBe('LABEL');
    expect(select.firstChild.getAttribute('value')).toBe('ascending');
    done();
  });

  it('should publish a filter event with the correct value', function(done) {
    var select = document.getElementById('sort');
    var evt = new CustomEvent('change');

    events.subscribe(events.types.filter.sort, function listener(e) {
      events.unsubscribe(events.types.filter.sort, listener);
      expect(e.detail.descending).toBe(true);
      done();
    });

    select.value = 'descending';
    select.dispatchEvent(evt);
  });

  afterAll(function() {
    document.body.innerHTML = '';
  });

});
