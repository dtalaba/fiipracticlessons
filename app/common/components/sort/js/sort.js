var dom = require('../../../utils/dom/js/dom.js');
var events = require('../../../events/js/events.js');
var strings = require('../../../utils/config/js/strings.js');

module.exports = (function SortComponent() {

  var init = function init() {
    dom({
      'type': 'div',
      'attributes': {
        'class': 'input__group'
      },
      'content': [
        dom({
          'type': 'label',
          'attributes': {
            'for': 'sort'
          },
          'content': strings.sort
        }),
        dom({
          'type': 'select',
          'attributes': {
            'class': 'sort__select',
            'name': 'sort',
            'id': 'sort'
          },
          'events': {
            'change': function onchange() {
              events.publish(events.types.filter.sort, {
                'descending': this.value === 'descending'
              });
            }
          },
          'content': [
            dom({
              'type': 'option',
              'attributes': {
                'value': 'ascending'
              },
              'content': strings.priceLowHigh
            }),
            dom({
              'type': 'option',
              'attributes': {
                'value': 'descending'
              },
              'content': strings.priceHighLow
            })
          ]
        })
      ]
    }, document.getElementById('sortSection'));
  };

  return {
    'init': init
  };

}());
