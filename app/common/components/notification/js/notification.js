var dom = require('../../../utils/dom/js/dom.js');
var html = require('../../../utils/dom/js/html.js');

var notificationComponent = (function notificationModule() {
  var show = function showNotification(message, parent, timeout) {
    var notification = null;

    notification = dom({
      'type': 'div',
      'attributes': {
        'class': 'notification'
      },
      'content': [
        dom({
          'type': 'p',
          'content': message
        })
      ]
    });

    parent.appendChild(notification);

    window.setTimeout(function removeNotification() {
      html.removeElement(notification);
    }, timeout || 1600);
  };

  return {
    'show': show
  };
}());

module.exports = notificationComponent;

