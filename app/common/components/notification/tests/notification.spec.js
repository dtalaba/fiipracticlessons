/* eslint-disable func-names */

var notification = require('../js/notification.js');


describe('Notification tests', function() {
  it('should work correctly', function(done) {
    var elem = null;
    notification.show('foo', document.body, 100);

    elem = document.querySelector('.notification');
    expect(elem).not.toBeNull();
    expect(elem.firstChild.innerHTML).toBe('foo');

    window.setTimeout(function removeNotification() {
      // we check that the notification disappeared
      expect(document.querySelector('.notification')).toBeNull();
      done();
    }, 200);
  });
});
