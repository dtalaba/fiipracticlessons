var events = require('../../../events/js/events.js');
var dom = require('../../../utils/dom/js/dom.js');
var mediator = require('../../../mediator/js/mediator.js');
var strings = require('../../../utils/config/js/strings.js');

module.exports = (function productCountModule() {

  var header = document.querySelector('.header__navigation');

  var createDOM = function createDOM() {
    dom({
      'type': 'a',
      'attributes': {
        'class': 'element--right',
        'href': 'pages/cart/html/cart.html'
      },
      'content': [
        dom({
          'type': 'span',
          'content': strings.cartName + '('
        }),
        dom({
          'type': 'span',
          'attributes': {
            'id': 'number__of__products'
          },
          'content': mediator.getDistinctCartProducts()
        }),
        dom({
          'type': 'span',
          'content': ')'
        }),
      ]
    }, header);
  };

  var updateCart = function updateCart() {
    var numberWrapper = document.getElementById('number__of__products');
    if (numberWrapper) {
      numberWrapper.innerHTML = mediator.getDistinctCartProducts();
    }
  };

  var init = function init() {
    events.subscribe(events.types.cart.load, createDOM);
    events.subscribe(events.types.cart, updateCart);
  };

  return {
    'init': init
  };

}());
