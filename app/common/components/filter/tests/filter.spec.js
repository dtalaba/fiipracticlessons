/* eslint-disable func-names */

var dom = require('../../../utils/dom/js/dom.js');
var filters = require('../js/filter.js');
var events = require('../../../events/js/events.js');

describe('Filter component tests', function() {

  var searchBar,
    searchButton,
    subscribeToFilterEvent,
    checkValues,
    filterSearchButton,
    priceFrom,
    priceTo;

  beforeAll(function() {
    // Required DOM mock creation
    dom({
      'type': 'section',
      'content': [
        searchBar = dom({
          'type': 'input',
          'attributes': {
            'class': 'searchInput'
          }
        }),
        searchButton = dom({
          'type': 'button',
          'attributes': {
            'id': 'searchButton'
          }
        })
      ]
    }, document.body);

    dom({
      'type': 'sections',
      'content': [
        priceFrom = dom({
          'type': 'input',
          'attributes': {
            'id': 'priceFrom',
            'type': 'number'
          }
        }),
        priceTo = dom({
          'type': 'input',
          'attributes': {
            'id': 'priceTo',
            'type': 'number'
          }
        }),
        filterSearchButton = dom({
          'type': 'button',
          'attributes': {
            'id': 'filterButton'
          }
        })
      ]
    }, document.body);

    subscribeToFilterEvent = function subscribeToFilterEvent(done) {
      events.subscribe(events.types.filter.search, function listener(e) {
        events.unsubscribe(events.types.filter.search, listener);
        expect(e.detail.eventName).toBe(events.types.filter.search);
        done();
      });
    };

    checkValues = function checkValues(e, min, max) {
      expect(e.detail.value[0]).toBe(min);
      expect(e.detail.value[1]).toBe(max);
    };
  });


  // Actual tests
  it('should initialize correctly', function() {
    filters.attachFilterEvents();
  });

  it('should publish an event when "Search" is pressed', function(done) {
    subscribeToFilterEvent(done);
    searchButton.click();
  });

  it('should publish an event when "Return" key is pressed (.which)',
    function(done) {
      var EnterKeyPress = new CustomEvent('keydown');
      subscribeToFilterEvent(done);
      EnterKeyPress.which = 13;
      searchBar.dispatchEvent(EnterKeyPress);
    }
  );

  it('should publish an event when "Return" key is pressed (.keyCode)',
    function(done) {
      var EnterKeyPress = new CustomEvent('keydown');
      subscribeToFilterEvent(done);
      EnterKeyPress.keyCode = 13;
      searchBar.dispatchEvent(EnterKeyPress);
    }
  );

  it('should match the input entered by the user', function(done) {
    var inputString = 'test string_#3';

    function checkString(e) {
      events.unsubscribe(events.types.filter.search, checkString);
      expect(e.detail.value).toBe(inputString);
      done();
    }

    events.subscribe(events.types.filter.search, checkString);

    window.setTimeout(function() {
      searchBar.value = inputString;
      searchButton.click();
    }, 200);
  });

  it('should do nothing when other key is pressed', function(done) {
    var listener = {
      'callback': function(e) {
        expect(e.detail.eventName).toBe(events.types.filter.search);
      }
    };

    var spy = spyOn(listener, 'callback'),
      EnterKeyPress = new CustomEvent('keydown');

    events.subscribe(events.types.filter.search, function searchListener(e) {
      events.unsubscribe(events.types.filter.search, searchListener);
      listener.callback(e);
    });

    EnterKeyPress.keyCode = 14;
    searchBar.dispatchEvent(EnterKeyPress);

    window.setTimeout(function() {
      expect(spy).not.toHaveBeenCalled();
      done();
    }, 200);
  });

  it('should handle invalid values (NaN)', function(done) {
    priceFrom.value = '-100';
    priceTo.value = 'invalid';

    function validate(e) {
      events.unsubscribe(events.types.filter.price, validate);
      checkValues(e, Number.MIN_VALUE, Number.MAX_VALUE);
      window.setTimeout(done.bind(this), 200);
    }

    events.subscribe(events.types.filter.price, validate);
    filterSearchButton.click();
  });

  it('should handle Infinity', function(done) {
    priceFrom.value = 'NaN';
    priceTo.value = 'Infinity';

    function validate(e) {
      events.unsubscribe(events.types.filter.price, validate);
      checkValues(e, Number.MIN_VALUE, Number.MAX_VALUE);
      window.setTimeout(done.bind(this), 200);
    }

    events.subscribe(events.types.filter.price, validate);
    filterSearchButton.click();
  });

  it('should handle bigger min than max', function(done) {
    priceFrom.value = '400';
    priceTo.value = '100';

    function validate(e) {
      events.unsubscribe(events.types.filter.price, validate);
      checkValues(e, Number.MIN_VALUE, Number.MAX_VALUE);
      window.setTimeout(done.bind(this), 200);
    }

    events.subscribe(events.types.filter.price, validate);
    filterSearchButton.click();
  });

  it('should handle valid values', function(done) {
    priceFrom.value = '100';
    priceTo.value = '300';

    function validate(e) {
      events.unsubscribe(events.types.filter.price, validate);
      checkValues(e, 100, 300);
      done();
    }

    events.subscribe(events.types.filter.price, validate);
    filterSearchButton.click();
  });

  afterAll(function() {
    document.body.innerHTML = '';
  });

});
