var events = require('../../../events/js/events.js');

module.exports = (function filterComponentModule() {

  var attachFilterEvents = function attachFilterEvents() {

    function handleSearchBar() {

      var searchBar = document.querySelector('.searchInput'),
        searchButton = document.getElementById('searchButton'),
        keyCode = null;

      function triggerSearch() {
        events.publish(events.types.filter.search, {
          'value': searchBar.value
        });
      }

      searchBar.onkeydown = function handleSearchBar(e) {

        // some browsers get the pressed key via ".which", other via ".keyCode"
        if(e.which) {
          keyCode = e.which;
        } else {
          keyCode = e.keyCode;
        }

        // check if user pressed "Return" after adding the input
        if (keyCode === 13) {
          triggerSearch();
        }
      };

      searchButton.addEventListener('click', triggerSearch);
    }

    function handleInterval() {

      var filterSearchButton = document.getElementById('filterButton'),
        priceFrom = document.getElementById('priceFrom'),
        priceTo = document.getElementById('priceTo'),
        min = null,
        max = null;

      function triggerInterval() {
        min = parseInt(priceFrom.value, 10);
        max = parseInt(priceTo.value, 10);

        if(isNaN(min) || isNaN(max) || (min > max)) {
          min = Number.MIN_VALUE;
          max = Number.MAX_VALUE;
        }

        events.publish(events.types.filter.price, {
          'value': [min, max]
        });
      }

      filterSearchButton.addEventListener('click', triggerInterval);
    }

    handleSearchBar();
    handleInterval();
  };

  return {
    'attachFilterEvents': attachFilterEvents
  };

}());
