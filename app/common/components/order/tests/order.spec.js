/* eslint-disable func-names */

var order = require('../js/order.js');
var events = require('../../../events/js/events.js');
var dom = require('../../../utils/dom/js/dom.js');
var mediator = require('../../../mediator/js/mediator.js');
var Product = require('../../../model/js/product.js');

describe('Order tests', function() {

  var tv, price;

  beforeAll(function() {
    // Required mocks creation
    var modal = dom({
      'type': 'div',
      'attributes': {
        'id': 'modal'
      }
    }, document.body);

    tv = new Product({
      'name': 'SmartTV',
      'id': 'foo',
      'specs': {
        'price': 199
      }
    });

    price = null;
    order.init(modal);
  });

  // Actual tests
  it('should subscribe to the cart order event', function(done) {
    var button = document.querySelector('#modal button');
    events.subscribe(events.types.cart.order, function listener(e) {
      events.unsubscribe(events.types.cart.order, listener);
      expect(e.detail.eventName).toBe(events.types.cart.order);
      done();
    });
    button.click();
  });

  it('should update the price after adding to cart', function(done) {
    var listener = function() {
      events.unsubscribe(events.types.cart.add, listener);
      price = parseInt(
        document.getElementById('priceValue').innerHTML,
        10
      );
      expect(price).toBe(mediator.getCartTotal());
      done();
    };
    events.subscribe(events.types.cart.add, listener);
    mediator.addToCart(tv);
  });

  it('should update the price after removing from cart', function(done) {
    var listener = function() {
      events.unsubscribe(events.types.cart.remove, listener);
      price = parseInt(
        document.getElementById('priceValue').innerHTML,
        10
      );
      expect(price).toBe(0);
      done();
    };
    events.subscribe(events.types.cart.remove, listener);
    mediator.removeCartProduct(tv);
  });

  afterAll(function() {
    document.body.innerHTML = '';
  });

});
