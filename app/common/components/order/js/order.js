var dom = require('../../../utils/dom/js/dom.js');
var events = require('../../../events/js/events.js');
var mediator = require('../../../mediator/js/mediator.js');
var strings = require('../../../utils/config/js/strings.js');


var orderComponent = (function orderComponent() {

  var init = function init(parent) {
    var priceDisplay = null;
    // price display
    dom({
      'type': 'h4',
      'content': strings.totalPrice + ':'
    }, parent);
    priceDisplay = dom({
      'type': 'h4',
      'attributes': {
        'id': 'priceValue'
      },
      'content': 0
    }, parent);
    dom({
      'type': 'h4',
      'content': '$'
    }, parent);

    // order button
    dom({
      'type': 'button',
      'events': {
        'click': function onClick() {
          events.publish(events.types.cart.order);
        }
      },
      'content': strings.order
    }, parent);

    events.subscribe(events.types.cart, function setTotal() {
      priceDisplay.innerHTML = mediator.getCartTotal();
    });
  };

  return {
    'init': init
  };

}());

module.exports = orderComponent;
