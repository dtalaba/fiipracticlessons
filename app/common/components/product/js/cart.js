var dom = require('../../../utils/dom/js/dom.js');
var html = require('../../../utils/dom/js/html.js');
var mediator = require('../../../mediator/js/mediator.js');


module.exports = (function productCartModule() {

  var createCartDOM = function createCartDOM(product) {

    return dom({
      'type': 'div',
      'attributes': {
        'class': 'product--list__box',
      },
      'content': [
        dom({
          'type': 'div',
          'attributes': {
            'class': 'product--list__image',
            'style': [
              'background-image: url("',
              product.getThumb(),
              '")'
            ].join(''),
          }
        }),
        dom({
          'type': 'div',
          'attributes': {
            'class': 'product__details',
          },
          'content': [
            dom({
              'type': 'h4',
              'attributes': {
                'class': 'product__title',
              },
              content: product.name
            }),
            dom({
              'type': 'h3',
              'attributes': {
                'class': 'product__price',
              },
              content: product.getPrice()
            }),
            dom({
              'type': 'div',
              'attributes': {
                'class': 'quantity-container',
                'data-id': product.id
              },
              'content':[
                dom({
                  'type': 'button',
                  'attributes': {
                    'class': 'decrement',
                  },
                  'content': '-',
                  'events': {
                    'click': function decQuantity() {
                      mediator.changeCartQuantity(product, -1);
                    }
                  }
                }),
                dom({
                  'type': 'input',
                  'attributes': {
                    'class': 'quantity',
                    'readonly': 'readonly',
                    'value': product.quantity
                  }
                }),
                dom({
                  'type': 'button',
                  'attributes': {
                    'class': 'increment'
                  },
                  'content': '+',
                  'events': {
                    'click': function incQuantity() {
                      mediator.changeCartQuantity(product, 1);
                    }
                  }
                })
              ]
            }),

          ]
        }),
        dom({
          'type': 'span',
          'attributes': {
            'class': 'product--list__delete fa fa-times',
            'data-id': product.id,
          },
          'events': {
            'click': function removeProduct() {
              mediator.removeCartProduct(product);
              html.removeElement(this.parentNode);
            }
          },
        }),
      ]
    });
  };


  return {
    'createCartDOM': createCartDOM,
  };

}());
