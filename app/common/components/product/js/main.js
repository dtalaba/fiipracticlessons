var dom = require('../../../utils/dom/js/dom.js');
var modal = require('../../../../pages//modal/js/modal.js');
module.exports = (function productModule() {

  var createDOM = function createDOM(product) {
    return dom({
      'type': 'div',
      'attributes': {
        'class': 'product'
      },
      'content': [
        dom({
          'type': 'div',
          'attributes': {
            'class': 'product__box',
          },
          'content': [
            dom({
              'type': 'div',
              'attributes': {
                'class': 'product__image',
                'data-id': product.id,
                'style': [
                  'background-image: url("',
                  product.getThumb(),
                  '")'
                ].join(''),
              },
              'events': {
                'click': function openModal() {
                  modal.open(product);
                }
              },
            }),
            dom({
              'type': 'div',
              'attributes': {
                'class': 'product__details',
              },
              'content': [
                dom({
                  'type': 'h4',
                  'attributes': {
                    'class': 'product__title',
                  },
                  content: product.name
                }),
                dom({
                  'type': 'h3',
                  'attributes': {
                    'class': 'product__price',
                  },
                  content: product.getPrice()
                })
              ]
            })
          ]
        })
      ]
    });
  };

  return {
    'createDOM': createDOM,
  };

}());
