/* eslint-disable func-names */

var events = require('../js/events.js');


describe('Events', function() {

  var types;
  beforeAll(function() {
    types = {
      'single': 'single',
      'multiple': {
        'one': 'one',
        'two': 'two'
      }
    };
  });

  it('should not fail due to incorrect event name', function(done) {
    events.subscribe(null);
    events.subscribe(undefined);
    events.subscribe('');
    events.subscribe([]);
    events.subscribe({});
    events.subscribe(0);
    done();
  });

  it('should subscribe and unsubscribe to events', function() {
    var value = 0;

    events.subscribe(types.single, function listener(e) {
      events.unsubscribe(types.single, listener);
      value += e.detail.value;
    });

    events.publish(types.single, {'value': 1});
    events.publish(types.single, {'value': 1});
    events.publish(types.single);

    expect(value).toBe(1);
  });

  it('should subscribe to multiple events', function() {
    var value = 0;
    events.subscribe(types.multiple, function listener(e) {
      events.unsubscribe(e.detail.eventName, listener);
      value += e.detail.value;
    });

    events.publish(types.multiple.one, {'value': 1});
    events.publish(types.multiple.two, {'value': 1});
    events.publish(types.multiple.one, {'value': 1});
    events.publish(types.multiple.two, {'value': 1});

    expect(value).toBe(2);
  });
});
