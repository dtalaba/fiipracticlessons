module.exports = (function eventsModule() {

  var types = {
    'products': {
      'retrieved': 'products.retrieved',
      'filtered': 'products.filtered',
      'cart': 'products.cart.retrieved',
    },
    'filter': {
      'search': 'filter.search',
      'price': 'filter.price',
      'category': 'filter.category',
      'sort': 'filter.sort'
    },
    'categories': {
      'retrieved': 'retrieved'
    },
    'cart': {
      'add': 'cart.add',
      'load': 'cart.load',
      'list': 'cart.list',
      'quantity': 'cart.quantity',
      'order': 'cart.order',
      'ordered': 'cart.ordered',
      'remove': 'cart.remove'
    }
  };

  var parseEventAction = function parseEventAction(eventName, listener, what) {
    var events = [];
    if (!eventName || ['string', 'object'].indexOf(typeof eventName) < 0) {
      return;
    }

    if (typeof eventName === 'string') {
      events.push(eventName);
    } else {
      events = Object.keys(eventName).map(function getEventName(key) {
        return eventName[key];
      });
    }

    events.forEach(function addListener(name) {
      what(name, listener);
    });
  };

  var subscribe = function subscribe(eventName, listener) {
    parseEventAction(eventName, listener, window.addEventListener.bind(window));
  };

  var publish = function publish(eventName, detail) {
    var e = null;
    detail = detail || {};
    detail.eventName = eventName;
    e = new CustomEvent(eventName, {'detail': detail});
    window.dispatchEvent(e);
  };

  var unsubscribe = function unsubscribe(eventName, listener) {
    parseEventAction(eventName, listener,
      window.removeEventListener.bind(window));
  };

  return {
    'subscribe': subscribe,
    'publish': publish,
    'types': types,
    'unsubscribe': unsubscribe
  };

}());
