/* eslint-disable func-names */

var net = require('../js/net.js');


describe('net tests', function() {

  it('should gracefuly fail when no parameters are sent', function(done) {
    net.xhr('http://google.com/', null, done);
  });

  it('should send error when request fails', function(done) {
    net.xhr('http://httpstat.us/400', null, function(e) {
      expect(e).not.toBeNull();
      done();
    });
  });

  it('should send request parameters', function(done) {
    var props = {
      'params': {
        'id': 1
      }
    };
    net.xhr('http://httpbin.org/get', props, function(error, data) {
      expect(error).toBeNull();
      expect(JSON.parse(data).args.id).toBe('1');
      done();
    });
  });

  it('should not fail when props sent with no request params', function(done) {
    var props = {
      'foo': {
        'id': 1
      }
    };
    net.xhr('http://httpbin.org/get', props, function(error, data) {
      expect(error).toBeNull();
      expect(data).not.toBeNull();
      done();
    });
  });

  it('should parse same key parameters', function(done) {
    var props = {
      'params': {
        'id': [1, 2]
      }
    };
    net.xhr('http://httpbin.org/get', props, function(error, data) {
      expect(error).toBeNull();
      expect(JSON.parse(data).args.id.length).toBe(2);
      done();
    });
  });

  it('should stringify object parameters', function(done) {
    var props = {
      'params': {
        'json': {'foo': 1}
      }
    };
    net.xhr('http://httpbin.org/get', props, function(error, data) {
      expect(error).toBeNull();
      expect(JSON.parse(JSON.parse(data).args.json).foo).toBe(1);
      done();
    });
  });

  it('should not JSON.parse string params', function(done) {
    var props = {
      'params': {
        'foo': 'bar'
      }
    };
    net.xhr('http://httpbin.org/get', props, function(error, data) {
      expect(JSON.parse(data).args.foo).toBe('bar');
      done();
    });
  });

  it('should not perform request when invalid param is sent', function(done) {
    var props = {
      'params': {
        'json': window,
      }
    };
    net.xhr('http://httpbin.org/get', props, function(error, data) {
      expect(error).not.toBeNull();
      expect(data).toBeUndefined();
      done();
    });
  });

  it('should not perform request when invalid params are sent', function(done) {
    var props = {
      'params': {
        'json': [1, window, 2],
      }
    };
    net.xhr('http://httpbin.org/get', props, function(error, data) {
      expect(error).not.toBeNull();
      expect(data).toBeUndefined();
      done();
    });
  });

});
