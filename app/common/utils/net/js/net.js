var _ = require('lodash');

module.exports = (function netModule() {

  var xhr = function xhr(url, props, cb) {
    var addToUrl = function addToUrl(key, value) {
      if (typeof value !== 'string') {
        try {
          value = JSON.stringify(value);
        } catch(e) {
          return false;
        }
      }
      url += [key, '=', value, '&'].join('');
      return true;
    };
    var i = 0;
    var l = 0;
    var request = null;

    props = props || {};
    request = new window.XMLHttpRequest();

    if (props.hasOwnProperty('params')) {
      url += '?';
      _.forOwn(props.params, function addParam(value, key) {
        if (_.isArray(value)) {
          for (i = 0, l = value.length; i < l; i++) {
            if (!addToUrl(key, value[i])) {
              cb(new Error('Invalid request parameters'));
              return false;
            }
          }
        } else {
          if(!addToUrl(key, value)) {
            cb(new Error('Invalid request parameters'));
            return false;
          }
        }
      });
    }

    // xhrReady used before defined...? This is called hoisting:
    // https://developer.mozilla.org/en-US/docs/Glossary/Hoisting
    request.onreadystatechange = xhrReady;

    function xhrReady() {
      if (request.readyState !== window.XMLHttpRequest.DONE) {
        return;
      }

      if (request.status === 200) {
        // We got a 200 OK, everything worked as expected
        cb(null, request.response);
      } else {
        cb(new Error(request.response));
      }
    };

    request.open('GET', url);
    request.send();
  };

  return {
    'xhr': xhr
  };

}());
