/* eslint-disable func-names */

var dom = require('../js/dom.js');


describe('DOM creation test', function() {

  it('should create complex DOM elements', function() {
    var element = dom({
      'type': 'div',
      'attributes': {
        'class': 'foo'
      },
      'events': {
        'click': function() {}
      },
      'content': [
        dom({
          'type': 'p',
          'content': 'foo'
        }),
        dom({
          'type': 'p',
          'content': 'bar'
        })
      ]
    });

    expect(element.nodeName).toBe('DIV');
    expect(element.className).toBe('foo');
    expect(element.firstChild.nodeName).toBe('P');
    expect(element.children[1].innerHTML).toBe('bar');
  });

  it('should append to existing DOM correctly', function() {
    var prnt = dom({
      'type': 'div'
    });

    dom({
      'type': 'p'
    }, prnt);

    expect(prnt.firstChild.nodeName).toBe('P');
  });

  it('should display numeric content', function() {
    var e = dom({
      'type': 'span',
      'content': 0
    });

    expect(e.innerHTML).toBe('0');
  });

});
