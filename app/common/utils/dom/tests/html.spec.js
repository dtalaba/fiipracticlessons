/* eslint-disable func-names */

var dom = require('../js/dom.js');
var html = require('../js/html.js');


describe('HTML.removeElement tests', function() {
  it('should remove valid elements', function() {
    var elem = dom({
      'type': 'div',
      'attributes': {
        'id': 'elementToRemove'
      }
    }, document.body);
    expect(document.getElementById('elementToRemove')).not.toBeNull();
    html.removeElement(elem);
    expect(document.getElementById('elementToRemove')).toBeNull();
  });

  it('should not throw when no argument is supplied', function() {
    html.removeElement();
  });

  it('should not throw when element has no parent', function() {
    var elem = dom({
      'type': 'div',
      'attributes': {
        'id': 'elementToRemove'
      }
    });
    html.removeElement(elem);
  });
});
