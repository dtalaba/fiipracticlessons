// nice collection of utilities, see https://lodash.com/docs
var _ = require('lodash');

module.exports = function makeDOM(props, parentNode) {
  var domElem = null;
  var container = null;
  var i = 0;
  var child = null;
  var n;

  // default missing properties
  var args = {
    type: props.type || null,
    attributes: props.attributes || {},
    events: props.events || {}
  };

  // normalize contents to array (one or more children of the created element)
  if (props.hasOwnProperty('content')) {
    if (_.isArray(props.content)) {
      args.content = props.content;
    } else {
      args.content = [props.content];
    }
  } else {
    args.content = [];
  }

  // create the actual element with the specified type (e.g., a DIV)
  domElem = document.createElement(args.type);

  // set the attributes (e.g., the class)
  _.each(args.attributes, function setAttr(v, k) {
    domElem.setAttribute(k, v);
  });

  // add some event listeners (e.g., onclick)
  _.each(args.events, function addEvent(v, e) {
    domElem.addEventListener(e, v);
  });

  // now add the contents of the new element
  container = document.createDocumentFragment();
  for (i = 0, n = args.content.length; i < n; i += 1) {
    if (['string', 'number'].indexOf(typeof args.content[i]) > -1) {
      // if the child is just some text, insert it in a proper text node; see
      // http://stackoverflow.com/a/25273968/418609
      child = document.createTextNode('' + args.content[i]);
    } else {
      // if the child is just another DOM element, just append it
      child = args.content[i];
    }
    container.appendChild(child);
  }
  domElem.appendChild(container);

  // if the second argument is supplied, we need to append the new Node to its
  // specified parent
  if (parentNode) {
    parentNode.appendChild(domElem);
  }

  return domElem;
};
