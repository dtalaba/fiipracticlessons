module.exports = (function htmlModule() {

  var removeElement = function removeElement(elem) {
    if (!elem || !elem.parentNode) {
      return;
    }
    elem.parentNode.removeChild(elem);
  };

  return {
    'removeElement': removeElement
  };

}());
