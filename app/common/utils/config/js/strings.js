module.exports = (function stringsModule() {
  return {
    'noProdsInCart': 'No products in your cart.',
    'ordered': 'Order placed!',
    'noProds': 'No products to show',
    'cartName': 'Cart',
    'totalPrice': 'Total price',
    'order': 'Order',
    'sort': 'Sort',
    'priceLowHigh': 'Price: Low to High',
    'priceHighLow': 'Price: High to Low'
  };
}());
