module.exports = (function configModule() {
  var apiUrl = 'http://localhost:4444';
  if (window.location.hostname.indexOf('herokuapp') > -1) {
    apiUrl = 'https://fiipractic.herokuapp.com';
  }
  return {
    'urls': {
      'api': apiUrl,
      'defaultImage': 'http://placehold.it/400x250'
    }
  };
}());
