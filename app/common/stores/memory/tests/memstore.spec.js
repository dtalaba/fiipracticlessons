/*eslint-disable func-names */

var MemStore = require('../js/memstore.js');


describe('memStorage tests', function() {

  var store;

  beforeAll(function() {
    // Required mocks creation
    store = Object.create(MemStore);
  });

  // Actual tests
  it('should gracefuly handle inexistent keys', function(done) {
    expect(store.get('foo')).toBeNull();
    done();
  });

  it('should fail for invalid JSON', function() {
    expect(function() { store.put('window', window); }).toThrow();
  });

});
