var MemStore = {
  'objects': {},

  'put': function put(key, value) {
    try {
      this.objects[key] = JSON.stringify(value);
    } catch(e) {
      throw e;
    }
    return this;
  },

  'get': function get(key) {
    if (!this.objects.hasOwnProperty(key)) {
      return null;
    }
    try {
      return JSON.parse(this.objects[key]);
    } catch(e) {
      throw e;
    }
  }
};

module.exports = MemStore;
