/* eslint-disable func-names */

var store = require('../js/lstorage.js');

describe('localStorage tests', function() {

  it('should gracefuly handle initial deserialize', function(done) {
    store.deserialize();
    expect(store.get('foo')).toBe(null);
    done();
  });

  it('should serialize/ deserialize', function(done) {
    store.put('foo', {'bar': 42});
    store.serialize();
    store.deserialize();
    expect(store.get('foo').bar).toBe(42);
    done();
  });

  it('should fail for invalid objects', function() {
    store.objects = {
      'window': window
    };
    expect(function () { store.serialize(); }).toThrow();
  });

});
