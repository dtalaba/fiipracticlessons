var MemStore = require('../../memory/js/memstore.js');

var store = Object.create(MemStore, {
  'name': {
    'value': 'lstorage'
  },

  'serialize': {
    'value': function serialize() {
      try {
        window.localStorage.setItem(this.name, JSON.stringify(this.objects));
      } catch(e) {
        throw e;
      }
      return this;
    }
  },

  'deserialize': {
    'value': function deserialize() {
      try {
        this.objects = JSON.parse(window.localStorage.getItem(this.name)) || {};
      } catch(e) {
        throw e;
      }
      return this;
    }
  }
});

module.exports = store;
