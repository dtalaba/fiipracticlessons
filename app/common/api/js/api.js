var config = require('../../utils/config/js/config.js');
var net = require('../../utils/net/js/net.js');
var Product = require('../../model/js/product.js');
var ProductList = require('../../model/js/list.js');


module.exports = (function apiModule() {

  var getCategories = function getCategories(cb, err) {
    net.xhr(config.urls.api + '/categories', {}, function done(e, data) {
      var categories = null;

      if (e || !data) {
        return err(e);
      }

      try {
        categories = JSON.parse(data);
      } catch(e) {
        err(e);
      }
      cb(categories);
    });
  };

  var getProducts = function getProducts(selectors, cb, err) {
    net.xhr(config.urls.api + '/products', {'params': selectors || {}},
      function done(e, data) {
        var products = null;

        if (e || !data) {
          return err(e);
        }

        try {
          products = JSON.parse(data);
        } catch(e) {
          err(e);
        }
        products = products.map(function model(json) {
          return new Product(json);
        });
        cb(new ProductList(products));
      }
    );
  };

  return {
    'getCategories': getCategories,
    'getProducts': getProducts
  };

}());
