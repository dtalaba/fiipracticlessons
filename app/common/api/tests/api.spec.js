/* eslint-disable func-names */

var _ = require('lodash');

var api = require('../js/api.js');
var config = require('../../utils/config/js/config.js');
var Product = require('../../model/js/product.js');
var ProductList = require('../../model/js/list.js');

/**
 * These tests expect the mock server to be running:
 *
 * $ gulp start-mock-server
 */

describe('API tests', function() {

  it('should retrieve categories', function(done) {
    var callbacks = {
      'error': function() {},

      'success': function(data) {
        expect(data).not.toBeNull();
        expect(_.isArray(data)).toBe(true);
        expect(data.length).toBe(2);

        // the error callback should obviously not be called when the request
        // succeeds
        expect(errSpy).not.toHaveBeenCalled();

        // this is a asynchronous test, so we need to manually signal when it
        // finished
        done();
      }
    };

    // a spy allows us to reflect on certain properties of the function (e.g.,
    // how many times it was called).
    var errSpy = spyOn(callbacks, 'error');

    api.getCategories(callbacks.success, callbacks.error);
  });

  it('should use error callback on categories', function(done) {
    var url = config.urls.api;

    var callbacks = {
      'error': function(e) {
        expect(e).not.toBeNull();

        // we restore the correct API url so next tests don't fail because of
        // this
        config.urls.api = url;

        expect(cbSpy).not.toHaveBeenCalled();
        done();
      },

      'success': function() {}
    };

    var cbSpy = spyOn(callbacks, 'success');

    // we change the API url and expect the request to fail
    config.urls.api = 'foo';

    api.getCategories(callbacks.success, callbacks.error);
  });

  it('should retrieve all products', function(done) {
    var callbacks = {
      'error': function() {},
      'success': function(data) {
        var gotOnlyProducts = true;

        // we check that we got a non-empty ProductList...
        expect(data).not.toBeNull();
        expect(data instanceof ProductList).toBe(true);
        expect(data.getLength() > 0).toBe(true);

        // ...which obviously contains only Products
        expect(errSpy).not.toHaveBeenCalled();
        _.forEach(ProductList.products, function(product) {
          if (!(product instanceof Product)) {
            gotOnlyProducts = false;
            return false;
          }
        });
        expect(gotOnlyProducts).toBe(true);

        expect(errSpy).not.toHaveBeenCalled();
        done();
      }
    };
    var errSpy = spyOn(callbacks, 'error');

    api.getProducts(null, callbacks.success, callbacks.error);
  });

  it('should retrieve products based on selectors', function(done) {
    var callbacks = {
      'error': function() {},
      'success': function(data) {
        var gotOnlyPhones = true;

        expect(data).not.toBeNull();
        expect(data instanceof ProductList).toBe(true);

        // we expect that we got Products only in the "phones" category
        expect(data.getLength() > 0).toBe(true);
        _.forEach(ProductList.products, function(product) {
          if (product.category !== 'phones') {
            gotOnlyPhones = false;
            return false;
          }
        });
        expect(gotOnlyPhones).toBe(true);

        expect(errSpy).not.toHaveBeenCalled();
        done();
      }
    };
    var errSpy = spyOn(callbacks, 'error');

    api.getProducts({'category': 'phones'}, callbacks.success, callbacks.error);
  });

  it('should not fail when no products retrieved', function(done) {
    var callbacks = {
      'error': function() {},
      'success': function(data) {
        // in this case we should receive an empty ProductList
        expect(data).not.toBeNull();
        expect(data instanceof ProductList).toBe(true);
        expect(data.getLength()).toBe(0);

        expect(errSpy).not.toHaveBeenCalled();
        done();
      }
    };
    var errSpy = spyOn(callbacks, 'error');

    api.getProducts({'category': 'foo'}, callbacks.success, callbacks.error);
  });

  it('should use error callback on products', function(done) {
    var url = config.urls.api;

    var callbacks = {
      'error': function(e) {
        expect(e).not.toBeNull();
        config.urls.api = url;

        expect(cbSpy).not.toHaveBeenCalled();
        done();
      },
      'success': function() {}
    };
    var cbSpy = spyOn(callbacks, 'success');

    config.urls.api = 'foo';
    api.getProducts(null, callbacks.success, callbacks.error);
  });

});
