var api = require('../../api/js/api.js');
var events = require('../../events/js/events.js');
var cart = require('../../model/js/cart.js');
var ProductList = require('../../model/js/list.js');

var Mediator = function MediatorConstructor() {
  this.allProducts = new ProductList();
  this.filters = {
    'search': '',
    'price': [Number.MIN_VALUE, Number.MAX_VALUE],
    'category': ''
  };
  this.descending = false;
  this.cart = cart;

  events.subscribe(events.types.filter, function filterListen(e) {
    switch (e.detail.eventName) {
    case events.types.filter.search:
      this.filters.search = e.detail.value.toLowerCase();
      break;
    case events.types.filter.category:
      if (this.filters.category === e.detail.value) {
        // we clear the already existing category filter
        this.filters.category = '';
      } else {
        this.filters.category = e.detail.value;
      }
      break;
    case events.types.filter.price:
      this.filters.price = e.detail.value;
      break;
    case events.types.filter.sort:
      this.descending = e.detail.descending;
    default:
      break;
    }


    // we chain all filters to yield the final result
    this.updateProducts(this.allProducts
      .filterByCategory(this.filters.category)
      .filterByName(this.filters.search)
      .filterByPrice(this.filters.price[0], this.filters.price[1])
      .sort(this.descending)
    );

    events.publish(events.types.products.filtered, this.filters);

  }.bind(this));

  events.subscribe(events.types.cart.order, function onOrder() {
    cart.order() && events.publish(events.types.cart.ordered);
  });
};

Mediator.prototype.getCategories = function getCategories() {
  api.getCategories(function done(categories) {
    events.publish(events.types.categories.retrieved, {
      'categories':  categories
    });
  }, console.error.bind(console));
};

Mediator.prototype.getProducts = function getProducts(selectors) {
  api.getProducts(selectors, function done(products) {
    this.allProducts = products;
    this.allProducts = this.allProducts.sort();
    this.updateProducts();
  }.bind(this), console.error.bind(console));
};

Mediator.prototype.updateProducts = function updateProducts(productList) {
  productList = productList || this.allProducts;
  events.publish(events.types.products.retrieved, {
    'products': productList,
  });
};

Mediator.prototype.addToCart = function addToCart(product) {
  // TODO: maybe send an addProduct fail event so we can notify the user?
  cart.add(product) && events.publish(events.types.cart.add);
};

Mediator.prototype.loadCart = function loadCart() {
  cart.load();
  events.publish(events.types.cart.load);
};

Mediator.prototype.getDistinctCartProducts = function getDistinctCartProds() {
  return cart.getDistinctProductsCount();
};

Mediator.prototype.removeCartProduct = function removeCartProduct(product) {
  cart.remove(product);
  events.publish(events.types.cart.remove, {
    'product': product
  });
};

Mediator.prototype.changeCartQuantity = function changeQty(product, change) {
  cart.changeQuantity(product, change);
  events.publish(events.types.cart.quantity, {
    'product': product,
  });
};

Mediator.prototype.getCartTotal = function getCartTotal() {
  return cart.getTotal();
};

Mediator.prototype.getProductFromCart = function getProductFromCart(id) {
  return this.cart.getProduct(id);
};

// notice how we export an instance and not the class; this will ensure that
// the whole application will use only a *single* instance of the Mediator class
module.exports = new Mediator();
