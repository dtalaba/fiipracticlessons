/* eslint-disable func-names */
var Product = require('../js/product.js');
var config = require('../../utils/config/js/config.js');

describe('Product tests', function() {

  var samsung, nexus, alcatel;

  beforeAll(function() {
    // Required mocks creation
    samsung = new Product({
      'name': 'Samsung Galaxy S7',
      'specs': {
        'price': '899'
      },
      'images': {
        'thumb': 'http://mygsm.me/images/cart-samsung/galaxy-s6/1.jpg'
      }
    });

    nexus = new Product({
      'name': 'Nexus 9',
      'specs': {
        'price': '799'
      }
    });

    alcatel = new Product({
      'name': 'Alcatel OT 311',
      'specs': {
        'price': '49'
      },
      'images': 'http://www.mobilityarena.com/wp-content/uploads/2010/12/1.jpg'
    });
  });

  // Actual tests
  it('should return the name of the product in lowercase', function(done) {
    expect(samsung.getNormalizedName()).toBe('samsung galaxy s7');
    done();
  });

  it('should return the price', function(done) {
    expect(samsung.getPrice()).toBe('899');
    done();
  });

  it('should return the thumb image', function(done) {
    expect(samsung.getThumb())
      .toBe('http://mygsm.me/images/cart-samsung/galaxy-s6/1.jpg');
    done();
  });

  it('should return the default image, if none other is specified',
    function(done) {
      expect(nexus.getThumb()).toBe(config.urls.defaultImage);
      done();
    }
  );

  it('should return the default image if no custom specified', function(done) {
    expect(alcatel.getThumb()).toBe(config.urls.defaultImage);
    done();
  });
});
