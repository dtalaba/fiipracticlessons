/* eslint-disable func-names */

var cart = require('../js/cart.js');
var Product = require('../js/product.js');

describe('Cart model tests', function() {
  it('should not allow adding invalid products to cart', function() {
    var noId, noPrice, noSpecs;

    noId = new Product({
      'name': 'productWithNoId',
      'specs': {'price': 100}
    });
    expect(cart.add(noId)).toBe(false);

    noSpecs = new Product({
      'name': 'productWithNoSpecs'
    });
    expect(cart.add(noSpecs)).toBe(false);

    noPrice = new Product({
      'name': 'productWithNoPrice',
      'id': 1,
      'specs': {}
    });
    expect(cart.add(noPrice)).toBe(false);

    expect(cart.getDistinctProductsCount()).toBe(0);
  });

});
