/* eslint-disable func-names */
var ProductList = require('../js/list.js'),
  Product = require('../js/product.js');

describe('Filter tests', function() {
  var list;

  beforeAll(function() {
    var products = [
      new Product({
        id: 'nexus6p',
        name: 'Nexus 6P',
        category: 'phones',
        specs: {
          'price': 10,
        }
      }),
      new Product({
        id: 'iphone5s',
        name: 'Apple Iphone 5s',
        category: 'phones',
        specs: {
          'price': 15,
        }
      }),
      new Product({
        id: 'iphone6',
        name: 'Apple Iphone 6',
        category: 'phones',
        specs: {
          'price': 5,
        }
      }),
      new Product({
        id: 'iphone6p',
        name: 'Apple Iphone 6 Plus',
        category: 'phones',
        specs: {
          'price': 25,
        }
      }),
      new Product({
        id: 'ipadmini2',
        name: 'Apple Ipad Mini 2',
        category: 'tablets',
        specs: {
          'price': 20,
        }
      }),
      new Product({
        id: 'galaxy3',
        name: 'Samsung galaxy 3',
        category: 'tablets',
        specs: {
          'price': 15,
        }
      })
    ];

    list = new ProductList(products);
  });

  it('should check validity on instantiation', function() {
    expect(list.getLength()).toBe(6);
  });

  it('should return sum for all prices from products', function(done) {
    var sumPrice = 0;

    list.forEach (function(product) {
      sumPrice += product.getPrice();
    });
    expect(sumPrice).toEqual(90);
    done();
  });

  it('should return ascending list products', function(done) {
    var descending = false,
      result = list.sort(descending);

    expect(result.products[0].id).toEqual('iphone6');
    expect(result.products[5].id).toEqual('iphone6p');
    done();
  });

  it('should return descending list products', function(done) {
    var descending = true,
      result = list.sort(descending);

    expect(result.products[0].id).toEqual('iphone6p');
    expect(result.products[0].getPrice()).toEqual(25);
    expect(result.products[1].id).toEqual('ipadmini2');
    done();
  });

  it('should return product listing by name', function(done) {
    var result = list.filterByName('apple');

    expect(result.products.length).toEqual(4);
    expect(result.products[0].name).toEqual('Apple Iphone 6 Plus');
    expect(result.products[1].name).toEqual('Apple Ipad Mini 2');
    expect(result.products[2].name).toEqual('Apple Iphone 5s');
    expect(result.products[3].name).toEqual('Apple Iphone 6');
    list = result;
    done();
  });

  it('should return products with price between min and max', function(done) {
    var min = 15,
      max = 30,
      result = list.filterByPrice(min, max);
    expect(result.products.length).toEqual(3);
    expect(result.products[0].id).toEqual('iphone6p');
    expect(result.products[2].getPrice()).toEqual(15);
    list = result;
    done();
  });

  it('should return all products if min and max doesn`t exist', function(done) {
    var result = list.filterByPrice(NaN, 1);
    expect(result.products.length).toEqual(list.products.length);
    done();
  });

  it('should call filterByCategory with no parameters ', function(done) {
    var result = list.filterByCategory();

    expect(result).not.toBeNull();
    expect(result.products.length).toEqual(3);
    expect(result.products[2].id).toEqual('iphone5s');
    done();
  });

  it('should call filterByCategory with valid parameters ', function(done) {
    var category = 'tablets',
      result = list.filterByCategory(category);

    expect(category).not.toBeNull();
    expect(result.products.length).toEqual(1);
    expect(result.products[0].category).toEqual('tablets');
    expect(result.products[0].name).toEqual('Apple Ipad Mini 2');
    done();
  });

});
