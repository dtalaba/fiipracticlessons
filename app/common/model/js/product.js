var config = require('../../utils/config/js/config');
var Product = function Product(props) {
  Object.keys(props).forEach(function addKey(key) {
    this[key] = props[key];
  }.bind(this));
};

Product.prototype.getNormalizedName = function getNormalizedName() {
  return this.name.toLowerCase();
};

Product.prototype.getPrice = function getPrice() {
  return this.specs.price;
};

Product.prototype.getThumb = function getThumb() {
  var result = config.urls.defaultImage;
  if (this.images && this.images.thumb) {
    result = this.images.thumb;
  }
  return result;
};


module.exports = Product;
