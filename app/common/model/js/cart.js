var store = require('../../stores/localstorage/js/lstorage.js');

var cartModule = (function cartModule() {

  var Cart = function Cart() {
    // we try to load an existing shopping cart from local storage
    this.load();
    this.save();
  };

  Cart.prototype.add = function addProduct(product) {
    if(!product.hasOwnProperty('id') ||
       !product.hasOwnProperty('specs') ||
       !product.specs.hasOwnProperty('price')) {
      return false;
    }

    if (!this.products.hasOwnProperty(product.id)) {
      this.products[product.id] = product;
      this.products[product.id].quantity = 0;
    }
    this.products[product.id].quantity += 1;
    return this.save();
  };

  Cart.prototype.remove = function removeProduct(product) {
    delete this.products[product.id];
    this.save();
  };

  Cart.prototype.changeQuantity = function changeQuantity(product, change) {
    if (!this.products.hasOwnProperty(product.id)) {
      return -1;
    }

    this.products[product.id].quantity += change;

    if (this.products[product.id].quantity <= 0) {
      // we don't allow decreasing the quantity below 1; the user will have to
      // explicitly remove the product
      this.products[product.id].quantity = 1;
    }

    this.save();

    return this.products[product.id].quantity;
  };

  Cart.prototype.forEach = function forEach(cb) {
    var key, product;
    for (key in this.products) {
      if (this.products.hasOwnProperty(key)) {
        product = this.products[key];
        cb(product);
      }
    }
  };

  Cart.prototype.getDistinctProductsCount = function getDistinctProducts() {
    var products = this.products,
      product = null,
      count = 0;

    for (product in products) {
      if (products.hasOwnProperty(product)) {
        count += 1;
      }
    }

    return count;
  };

  Cart.prototype.save = function save() {
    try {
      store.put('cart', this.products);
      store.serialize();
      return true;
    } catch(e) {
      return false;
    }
  };

  Cart.prototype.load = function load() {
    store.deserialize();
    this.products = store.get('cart') || {};
  };

  Cart.prototype.order = function order() {
    if (this.getDistinctProductsCount()) {
      this.forEach(this.remove.bind(this));
      return true;
    }
    return false;
  };

  Cart.prototype.getTotal = function getTotal() {
    var result = 0;
    var key, product;
    for (key in this.products) {
      if (this.products.hasOwnProperty(key)) {
        product = this.products[key];
        result += product.quantity * product.specs.price;
      }
    }
    return result;
  };

  Cart.prototype.getProduct = function getProduct(id) {
    return this.products[id];
  };

  return {
    'Cart': Cart
  };

}());

// we use a singleton instance of the shopping cart
module.exports = new cartModule.Cart();
