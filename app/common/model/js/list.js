var _ = require('lodash');
var ProductList = function ProductList(products) {
  this.products = products || [];
};

ProductList.prototype.filterByName = function filterByName(query) {
  if (!query) {
    return this;
  }
  return new ProductList(this.products.filter(function filter(product) {
    return product.getNormalizedName().indexOf(query) > -1;
  }));
};

ProductList.prototype.filterByPrice = function filterByPrice(min, max) {
  if (!(_.isFinite(min) && _.isFinite(max) && (min < max) && (min >= 0))) {
    return this;
  }
  return new ProductList(this.products.filter(function filter(product) {
    return (product.getPrice() >= min) && (product.getPrice() <= max);
  }));
};

ProductList.prototype.filterByCategory = function filterByCategory(category) {
  if (!category) {
    return this;
  }
  return new ProductList(this.products.filter(function filter(product) {
    return product.category === category;
  }));
};

ProductList.prototype.sort = function sort(descending) {
  this.products.sort(function compare(a, b) {
    return a.getPrice() - b.getPrice();
  });
  if (descending) {
    this.products.reverse();
  }
  return new ProductList(this.products);
};

ProductList.prototype.forEach = function forEach(cb, thisArg) {
  thisArg = thisArg || this;
  this.products.forEach(cb, thisArg);
};

ProductList.prototype.getLength = function length() {
  return this.products.length;
};


module.exports = ProductList;
