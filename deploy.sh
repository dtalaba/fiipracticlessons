#!/bin/bash
set -o nounset
set -o errexit

rm -rf ./public/*
cp ./build/* ./public/ -R
