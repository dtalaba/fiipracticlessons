To run:

$ gulp  # prepares everything
$ npm run webpack  # runs the webpack server which also live-reloads the JS
$ npm run server  # runs the mock server
